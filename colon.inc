%define PREV_PTR 0      ; null ptr, указывающий, что следующего элемента нет

%macro colon 2          ; макрос, принимает два агрумента
    ; Проверка 1-го и 2-го аргументов.
    %ifnstr %1
        %error "Error: The first element of the colon macro must be a string."
    %endif
    %ifnid %2
        %error "Error: The second agrument of the colon macro must be a label."
    %endif
    %2:                 ; на второй аргумент (метку)
        dq PREV_PTR     ; выделяем память на указатель на прошлый элемент
        db %1, 0        ; и выделяем память на ключ
    %define PREV_PTR %2 ; переопределяем метку на прошлый элемент
%endmacro