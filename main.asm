%include "words.inc"
%include "dict.inc"
%include "lib.inc"

%define PTR_SIZE 8          ; размер адреса метки
%define BUFF_SIZE 256       ; размер буфера ввода символов
%define SYS_WRITE 1         ; код системного вызова sys_write
%define STDERR 2            ; поток ошибок STRERR

section .rodata             ; секция для чтения (сообщения ошибок)
wrong_key_error: db "Error: Value with this key does not exist!", 0
long_key_error: db "Error: Key is too long!", 0

section .bss                ; секция изменяемых глобальных переменных
input_buffer: resb BUFF_SIZE; инициализируем input_buffer 256 нулями
KEY_ERROR: resq 1           ; данная метка будет содержать указатель на сообщение ошибки

section .text

global _start

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin
; Останавливается и возвращает 0 если строка слишком большая для буфера
; При успехе возвращает адрес буфера в rax, длину строки в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к строке нуль-терминатор
read_string:
	xor		rcx, rcx		; обнуляем rcx (кол-во прочитанных символов)
	push	r12				; сохраняем регистры r12, r13, r14, потому что мы будем их использовать
	push	r13				; для временного хранения значений rdi, rsi и rcx
	push	r14				; они callee-saved, что позволяет нам так сделать

	.loop:
		mov		r12, rdi	; сохраняем в r12 rdi (изменяется функцией)
		mov		r13, rsi	; сохраняем в r13 rsi (изменяется функцией)
		mov		r14, rcx	; сохраняем в r14 rcx (изменяется функцией)
		call	read_char	; читаем символ с помощью read_char
		mov		rcx, r14	; возвращаем из r14 rcx
		mov		rsi, r13	; возвращаем из r13 rsi
		mov		rdi, r12	; возвращаем из r12 rdi
		
        cmp		al, 0xA		; сравниваем символ с переносом строки
		jz		.return		; если они равны то прыгаем в .return
		cmp		al, 0		; сравниваем символ с нуль-терминатором
		jz		.return		; если они равны то прыгаем в .return

		cmp		rsi, rcx	; сравниваем кол-во прочитанных символов с размером буфера
		jz		.fail		; если они равны, то переходим в .fail (нет места для символов)

		mov		[rdi + rcx], rax	; сохраняем прочитанный символ в ячейку с адресом rdi + rcx
		inc		rcx					; увеличиваем кол-во прочитанных символов

		jmp		.loop		; прыгаем в начало .loop

	.return:
		pop		r14			; возвращаем из стека r14
		pop		r13			; возвращаем из стека r13
		pop		r12			; возвращаем из стека r12
		mov		rdx, rcx	; в rdx помещаем кол-во прочитанных символов
		mov		rax, rdi	; в rax помещаем адрес начала слова
		ret		

	.fail:
		pop		r14			; возвращаем из стека r14
		pop		r13			; возвращаем из стека r13
		pop		r12			; возвращаем из стека r12
		xor		rax, rax	; обнуляем rax
		ret

print_error:
	; rdi уже сожержит указатель на сообщение об ошибке
    call string_length  ; считаем длину этого сообщения

    mov rdx, rax        ; помещаем эту длину в rdx
    mov rax, SYS_WRITE  ; системный вызов sys_write
	mov rsi, rdi		; в rsi помещаем указатель на это сообщение
    mov rdi, STDERR     ; поток ошибок (сообщение об ошибке будет выводиться именно туда)
    syscall
    ret

_start:
    mov rdi, input_buffer   ; помещаем в rdi метку на буфер ввода
    mov rsi, BUFF_SIZE      ; в rsi записываем его длину
    call read_string        ; считываем строку! (не слово!, rdi, rsi не сохраняются)

    mov rdi, long_key_error    ; определяем сообщение об ошибке для длинного ключа
    test rax, rax           ; если ключ не поместился по размеру (rax = 0)
    jz .fail                ; прыгаем в .fail

    cmp rdx, BUFF_SIZE
    jae .fail

    mov rdi, input_buffer   ; помещаем в rdi метку на буфер ввода
    mov rsi, DICT_PTR       ; в rsi помещаем указатель на начало словаря
    call find_word          ; ищем слово по принятому ключу

    mov rdi, wrong_key_error; определяем сообщение об ошибке для несуществующего ключа
    test rax, rax           ; если слово не найдено (rax = 0)
    jz .fail                ; прыгаем в .fail

    mov rdi, rax            ; иначе же помещаем в rdi rax
    call get_dict_value_ptr ; и получаем значение в словаре по данному ключу

    mov rdi, rax            ; метку на начало значения помещаем в rdi 
    call print_string       ; и выводим значение

    jmp .exit               ; прыгаем в .exit

    .fail:
        call print_error    ; вызываем print_error
    
    .exit:
        call exit           ; выходим из программы