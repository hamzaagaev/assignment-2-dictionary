ASM=nasm
ASM_FLAGS=-f elf64
OUT=-o
LD=ld

PYTHON=python3
UNITTEST=unittest
TEST_SCRIPT=test_dictionary.py

%.o: %.asm
	$(ASM) $(ASM_FLAGS) $(OUT) $@ $<

main: main.o dict.o lib.o
	$(LD) $(OUT) $@ $^

.PHONY: test clean
test:
	@ $(PYTHON) -m $(UNITTEST) $(TEST_SCRIPT)

clean:
	rm -f *.o main