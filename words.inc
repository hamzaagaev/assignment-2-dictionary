; Использование макроса colon для добавления новых пар в связанный список (словарь)
%include "colon.inc"

section .data

colon "Java", Java
db "An object-oriented programming language commonly used for building scalable web applications.", 0

colon "C++", CPlusPlus
db "A versatile programming language that provides high performance and extensive development capabilities.", 0

colon "JavaScript", JavaScript
db "A programming language used for creating interactive web pages and dynamic web development.", 0 

colon "Python", Python
db "A simple and easy-to-understand programming language, popular in many development domains.", 0

; метка на начало вхождения в словарь
%define DICT_PTR Python