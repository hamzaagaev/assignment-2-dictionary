import unittest
from subprocess import PIPE, Popen

EXEC_FILE = "./main"
ASSERT_MESSAGE = "\n\nTEST FAILED: \ninput={0}, \n\nGOT: \noutput={1}, \nerrors={2}, \n\nEXPECTED: \noutput={3}, \nerrors={4}."

class DictionaryTest(unittest.TestCase):
    def test_right_output(self):
        inputs = [b"Java", b"C++", b"JavaScript", b"Python"]
        outputs = [
            b"An object-oriented programming language commonly used for building scalable web applications.",
            b"A versatile programming language that provides high performance and extensive development capabilities.",
            b"A programming language used for creating interactive web pages and dynamic web development.",
            b"A simple and easy-to-understand programming language, popular in many development domains."
        ]
        for i in range(len(inputs)):
            test_subpr = Popen(EXEC_FILE, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            out_subpr, err_subpr= test_subpr.communicate(input=inputs[i])
            AS_MSG = ASSERT_MESSAGE.format(inputs[i], out_subpr, err_subpr, outputs[i], b"")
            self.assertEqual(out_subpr, outputs[i], AS_MSG)
            self.assertEqual(err_subpr, b"", AS_MSG)

    def test_wrong_input(self):
        inputs = [b"PHP", b"C++ 20", b"-" * 255, b"1" * 256, b"." * 300]
        errors = [
            b"Error: Value with this key does not exist!",
            b"Error: Value with this key does not exist!",
            b"Error: Value with this key does not exist!",
            b"Error: Key is too long!",
            b"Error: Key is too long!"
        ]
        for i in range(len(inputs)):
            test_subpr = Popen(EXEC_FILE, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            out_subpr, err_subpr = test_subpr.communicate(input=inputs[i])
            AS_MSG = ASSERT_MESSAGE.format(inputs[i], out_subpr, err_subpr, b"", errors[i])
            self.assertEqual(out_subpr, b"", AS_MSG)
            self.assertEqual(err_subpr, errors[i], AS_MSG)